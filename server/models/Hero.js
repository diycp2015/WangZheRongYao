const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    //称号
    avatar: {
        type: String
    },
    //宣传图
    banner: {
        type: String
    },
    // 英雄称号
    title: {
        type: String
    },
    //英雄类型,使用数组，这样一个对象就可以关联多个分类
    categories: [{
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Category'
    }],
    //属性评分
    scores: {
        difficult: {
            type: Number
        },
        skills: {
            type: Number
        },
        attack: {
            type: Number
        },
        survive: {
            type: Number
        }
    },
    //技能
    skills: [{
        icon: {
            type: String
        },
        name: {
            type: String
        },
        description: {
            type: String
        },
        tips: {
            type: String
        },
        delay:{
            type: String
        },
        cost:{
            type: String
        }
    }],
    //装备（顺风，逆风）
    items1: [{
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Item'
    }],
    items2: [{
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Item'
    }],
    //使用技巧
    usageTips: {
        typeL: String
    },
    //对战技巧
    battleTips: {
        typeL: String
    },
    //团战思路
    teamTips: {
        typeL: String
    },
    //搭配英雄+原因
    partners: [{
        hero: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'Hero'
        },
        description: {
            type: String
        }
    }]
})

// 定义数据库的集合名heroes
module.exports = mongoose.model('Hero', schema, 'heroes')