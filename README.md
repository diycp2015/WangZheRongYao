# 王者荣耀全栈开发
部分截图
![输入图片说明](https://images.gitee.com/uploads/images/2020/0420/182135_13b694a5_4964818.png "NAG1AQN0%5MG@L}R.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0420/182323_789567f4_4964818.png "5I12Y9O{OFE50~CGLO]AYSB_看图王.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0420/182445_259638cf_4964818.png "YILW6Z$UEHQPM(JV](B%RJ.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0420/182453_4089f826_4964818.png "032RSW2(GS3(IO9HLCT2{FK.png")
## 部署到服务器上，localhost:3000地址是前台首页不是后台的首页，添加admin才是后台的首页

#### 1、admin是后台界面部分、web是前台界面部分、server是前后台的后端部分

#### 2、sever目录的uploads文件夹，用来存放图片资源，建议重新编辑所有的图标/图片，保存在你自己本地的数据库里

#### 3、使用技术（前台与后台的前后端开发）

```js
Vue+Element+nodejs+express+MongoDB+Mongoose
```
### 4、通用接口的类名转换成数据库一致的方法使用是inflection,把小写的复数名称改成大写开头的单数类名（也可以用lodash去尝试）

### 5、处理文件需要安装专门用来上传文件的中间件multer ,在server的anmin文件夹中的index.js配置。

### 6、上传的文件使用静态文件托管，存放在uploads文件夹，需要在server的主目录下的index.js定义静态路由（绝对路径）

### 7、一般前端的图片等样式文件也要使用静态路由，保证路径的正确（开放出这个路径）

### 8、富文本编辑器使用github上的 vue2-editor 可直接npm下载

### 9、用户的密码使用了比MD5（彩虹表hash碰撞40s就gg）加密程度更高的 "Bcrypt"（彩虹表hash碰撞12年）,每次生成的值都不一样，没有映射关系，安全性高。建议设置加密程度10（建议10~12）,否则时间过长

### 10、数据表密码设置select为false不可读取，校验密码时候需要使用
```js
select('+password')
```

### 11、服务端报错使用http-assert，能够很方便的返回错误

### 12、Web界面中的swiper的点击和滑动控制样式功能
```js
//点击转跳区域
@click = "$refs.list.swiper.slideTo(i)"
//区域改变，active的样式也跟随变化
@slide-change="() => active = $refs.list.swiper.realIndex"
```
### 12、格式化时间用的是dayjs

### 13、 使用jsonwebtoken进行登陆数据传输