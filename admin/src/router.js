import Vue from 'vue'
import Router from 'vue-router'
import Main from './views/Main.vue'
import CategoryEdit from './views/CategoryEdit.vue'
import categoryList from './views/CategoryList.vue'
import ItemEdit from './views/ItemEdit.vue'
import ItemList from './views/ItemList.vue'
import HeroEdit from './views/HeroEdit.vue'
import HeroList from './views/HeroList.vue'
import ArticleEdit from './views/ArticleEdit.vue'
import ArticleList from './views/ArticleList.vue'
import AdList from './views/AdList.vue'
import AdEdit from './views/AdEdit.vue'
import Admin_userList from './views/AdminUserList.vue'
import Admin_userEdit from './views/AdminUserEdit.vue'
import Login from './views/Login.vue'

Vue.use(Router)

// 防止多次点击路由报错
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

const router =  new Router({
  routes: [
    {
      path: '/',
      name: 'main',
      component: Main,
      meta: {
        isbg: false,
      },
      children:[
        { 
          path:'/categories/list',
          name: 'categoryList',
          component: categoryList
        },
        { 
          path:'/categories/create',
          name: 'categoryEdit',
          component: CategoryEdit
        },
        {
          path:'/categories/edit/:id',
          component:  CategoryEdit,
          props: true  //允许参数注入
        },
        // 装备
        { 
          path:'/items/list',
          name: 'ItemList',
          component: ItemList
        },
        { 
          path:'/items/create',
          name: 'ItemEdit',
          component: ItemEdit
        },
        {
          path:'/items/edit/:id',
          component:  ItemEdit,
          props: true  
        },
        //英雄
        { 
          path:'/heroes/list',
          name: 'HeroList',
          component: HeroList
        },
        { 
          path:'/heroes/create',
          name: 'HeroEdit',
          component: HeroEdit
        },
        {
          path:'/heroes/edit/:id',
          component:  HeroEdit,
          props: true    
        },
        // 文章区域
        { 
          path:'/articles/list',
          name: 'ArticleList',
          component: ArticleList
        },
        { 
          path:'/articles/create',
          name: 'ArticleEdit',
          component: ArticleEdit
        },
        {
          path:'/articles/edit/:id',
          component:  ArticleEdit,
          props: true 
        },
        // 广告区域
        { 
          path:'/ads/list',
          name: 'AdList',
          component: AdList
        },
        { 
          path:'/ads/create',
          name: 'AdEdit',
          component: AdEdit
        },
        {
          path:'/ads/edit/:id',
          component:  AdEdit,
          props: true 
        },
        // 管理员
        { 
          path:'/admin_users/list',
          name: 'Admin_userList',
          component: Admin_userList
        },
        { 
          path:'/admin_users/create',
          name: 'Admin_userEdit',
          component: Admin_userEdit
        },
        {
          path:'/admin_users/edit/:id',
          component:  Admin_userEdit,
          props: true 
        }
      ]
    },
    {
      path: '/login',
      name: 'login', 
      component: Login,
      meta: {
        isPublic: true,
      }
    }
  ]
})

// 导航守卫
router.beforeEach( (to,from,next)=>{
  if(!to.meta.isPublic && !sessionStorage.token){
    return next('/login')
  }
  next()
})

export default router